package com.poker_project.manegedbeans;

import java.sql.SQLException;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

//import com.poker_project.dao.DAO;
import com.poker_project.dao.JogadorDAO;
import com.poker_project.model.Jogador;
import com.poker_project.model.Torneio;
 

@ManagedBean(name="JogadorMB")
public class JogadorManagedBean {
     
     private Jogador jogador = new Jogador();
     private Torneio torneio = new Torneio();
     private int id_torneio;
     
     public void init() {
    	 FacesContext fc = FacesContext.getCurrentInstance();
    	 ExternalContext ec = fc.getExternalContext();
    	 Map<String,String> params = ec.getRequestParameterMap();
    	 String id = params.get("id");
    	 id_torneio = Integer.parseInt(id);
     }
     
     public String cadastraJogador() throws SQLException {
          
   		System.out.println("Teste aqui!!!");
	    JogadorDAO conexao = new JogadorDAO();         
	    
	    torneio.setId(id_torneio);
	    
	    if (conexao.inserirjogador(jogador, torneio)) {
	         FacesContext.getCurrentInstance().addMessage(
	          null, new FacesMessage(FacesMessage.SEVERITY_INFO,
	          "Sucesso!", "Jogador cadastrado com sucesso!"));
	    } else {
	         FacesContext.getCurrentInstance().addMessage(
	            null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Erro!", 
	            "Erro no cadastro do jogador!"));
	 
	                }
            //    conexao.closeConexao();
                
                
                
          return "";
     }

	public Jogador getJogador() {
		return jogador;
	}

	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}

	public int getId_torneio() {
		return id_torneio;
	}

	public void setId_torneio(int id_torneio) {
		this.id_torneio = id_torneio;
	}
     
//     public List<Cadastro> getCadastros() throws SQLException {
// 
//          Conexao con = new Conexao();
//          List<Cadastro> listaCadastros = con.listaCadastros();
//     
//          return listaCadastros;
//     }
//     
//     public Cadastro getCadastro() {
//          return cadastro;
//     }
//     
//     public void setCadastro(Cadastro cadastro) {
//          this.cadastro = cadastro;
//     }
     
     
     
     
}
	
	
	
//}
