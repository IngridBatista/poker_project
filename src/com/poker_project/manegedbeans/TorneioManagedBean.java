package com.poker_project.manegedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.poker_project.model.Torneio;
import com.poker_project.dao.TorneioDAO;
import java.sql.SQLException;
import java.util.List;

@ManagedBean(name="TorneioMB")
public class TorneioManagedBean {
	
	     private Torneio torneio = new Torneio();
	     
	     public String cadastraTorneio() throws SQLException {
	          
	  	   		System.out.println("Teste aqui!!!");
	                TorneioDAO conexao = new TorneioDAO();         
	                
	                
	                if (conexao.inserirtorneio(torneio)) {
	                     FacesContext.getCurrentInstance().addMessage(
	                      null, new FacesMessage(FacesMessage.SEVERITY_INFO,
	                      "Sucesso!", "torneio cadastrado com sucesso!"));
	                } else {
	                     FacesContext.getCurrentInstance().addMessage(
	                        null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Erro!", 
	                        "Erro no cadastro do torneio!"));
	 
	                }      
	                
	                
	          return "";
	     }
	
	     
     public List<Torneio> getTorneios() throws SQLException {
	
          TorneioDAO conexao = new TorneioDAO();
	          List<Torneio> listaTorneios = conexao.listartorneio();
	     
          return listaTorneios;
	     }
	      
	     
     public Torneio gettorneio() {
			return torneio;
		}

		public void settorneio(Torneio torneio) {
			this.torneio = torneio;
		}
     
	}
		
		
		

	
	
