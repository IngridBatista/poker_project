package com.poker_project.model;

import java.io.Serializable;

public class Torneio implements Serializable  {
	
	private static final long serialVersionUID = -309513637403441918L;
	
	private long id;
	private String nomeTorneio;
	private String descricaoTorneio;
	private String premioTorneio;
	private String localTorneio;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNomeTorneio() {
		return nomeTorneio;
	}
	public void setNomeTorneio(String nomeTorneio) {
		this.nomeTorneio = nomeTorneio;
	}
	public String getDescricaoTorneio() {
		return descricaoTorneio;
	}
	public void setDescricaoTorneio(String descricaoTorneio) {
		this.descricaoTorneio = descricaoTorneio;
	}
	public String getPremioTorneio() {
		return premioTorneio;
	}
	public void setPremioTorneio(String premioTorneio) {
		this.premioTorneio = premioTorneio;
	}
	public String getLocalTorneio() {
		return localTorneio;
	}
	public void setLocalTorneio(String localTorneio) {
		this.localTorneio = localTorneio;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
