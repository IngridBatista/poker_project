package com.poker_project.model;

import java.io.Serializable;

//import javax.persistence.Entity;
//import javax.persistence.Id;

//@Entity
public class Jogador implements Serializable {
	
//	@Id
	
private static final long serialVersionUID = -309513637403441918L;
	
	private long id;
	private long id_torneio;
	private String nomeJogador;
	private String cpf;
	private String nomeFantasiaJogador;
	private String telefone;
	private String email;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId_torneio() {
		return id_torneio;
	}
	public void setId_torneio(long id_torneio) {
		this.id_torneio = id_torneio;
	}
	public String getNomeJogador() {
		return nomeJogador;
	}
	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNomeFantasiaJogador() {
		return nomeFantasiaJogador;
	}
	public void setNomeFantasiaJogador(String nomeFantasiaJogador) {
		this.nomeFantasiaJogador = nomeFantasiaJogador;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

}
