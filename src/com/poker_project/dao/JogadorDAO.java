package com.poker_project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.poker_project.model.Jogador;
import com.poker_project.model.Torneio;


		 
	public class JogadorDAO extends DAO {
		boolean retorno;
	 
		public void alterarJogador(Jogador jogador) {
			try {
				Connection conexao = getConexao();
				PreparedStatement pstmt = conexao
						.prepareStatement("UPDATE jogador SET nome_jogador = ?, cpf = ?, nome_fantasiaJogador = ?, telefone = ?, email = ?" +
						 " WHERE id = ?");
						 
				                  pstmt.setLong(1, jogador.getId());  
				                  pstmt.setString(2, jogador.getNomeJogador());
				                  pstmt.setString(3, jogador.getCpf());
				                  pstmt.setString(4, jogador.getNomeFantasiaJogador());
								  pstmt.setString(5, jogador.getTelefone());
								  pstmt.setString(6, jogador.getEmail());
					
				pstmt.execute();
				pstmt.close();
				conexao.close();
	 
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	 
		public void excluirjogador(Jogador jogador) {
			try {
				Connection conexao = getConexao();
				PreparedStatement pstm = conexao
						.prepareStatement("Delete from	jogador where id = ? ");
				pstm.setLong(1, jogador.getId());
				pstm.execute();
				pstm.close();
				conexao.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	 
		public boolean existeJogador(Jogador jogador) {
			boolean achou = false;
			try {
				Connection conexao = getConexao();
				PreparedStatement pstm = conexao
						.prepareStatement("Select * from jogador where id =	?");
				pstm.setLong(1, jogador.getId());
				ResultSet rs = pstm.executeQuery();
				if (rs.next()) {
					achou = true;
				}
				pstm.close();
				conexao.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return achou;
		}
	 
		public boolean inserirjogador(Jogador jogador, Torneio torneio) {
			
			try {
				
				Connection conexao = getConexao();
				PreparedStatement pstm = conexao
						.prepareStatement("insert into jogador(id_torneio, nome_jogador, cpf, nome_fantasia, telefone, email)"
			               		+ "values(?,?,?,?,?,?)");
				pstm.setLong(1, torneio.getId());
				pstm.setString(2, jogador.getNomeJogador());
	            pstm.setString(3, jogador.getCpf());
	            pstm.setString(4, jogador.getNomeFantasiaJogador());
	            pstm.setString(5, jogador.getTelefone());
	            pstm.setString(6, jogador.getEmail());
	            
	            pstm.execute();
				pstm.close();
				conexao.close();
				
				return true;
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
	 
		public List<Jogador> listarJogador() {
			List<Jogador> lista = new ArrayList<Jogador>();
			try {
				Connection conexao = getConexao();
				Statement stm = conexao.createStatement();
				ResultSet rs = stm.executeQuery("Select * from jogador");
				while (rs.next()) {
					 Jogador jogador = new Jogador();
	                 jogador.setId(rs.getLong("id"));
	                 jogador.setNomeJogador(rs.getString(2));
	                 jogador.setCpf(rs.getString(3));
	                 jogador.setNomeFantasiaJogador(rs.getString(4));
	                 jogador.setTelefone(rs.getString(5));
	                 jogador.setEmail(rs.getString(7));				
	                 lista.add(jogador);
				}
				stm.close();
				conexao.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return lista;
		}
	 
		public Jogador consultarJogador(Jogador jogador) {
			try {
				Connection conexao = getConexao();
				PreparedStatement pstm = conexao
						.prepareStatement("Select * from jogador where id =	?");
				pstm.setLong(1, jogador.getId());
				ResultSet rs = pstm.executeQuery();
				if (rs.next()) {
					jogador.setId(rs.getLong("id"));
					jogador.setNomeJogador(rs.getString("nome_jogador"));
					jogador.setCpf(rs.getString("cpf"));
					jogador.setNomeFantasiaJogador(rs.getString("nome_fantasia"));
					jogador.setTelefone(rs.getString("telefone"));
					jogador.setEmail(rs.getString("email"));
				}
				
				pstm.close();
				conexao.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jogador;
		}
	}

	

	
	
