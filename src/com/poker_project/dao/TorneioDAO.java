package com.poker_project.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.poker_project.model.Torneio;

public class TorneioDAO extends DAO {

			boolean retorno;
		 
			public void alterartorneio(Torneio torneio) {
				try {
					Connection conexao = getConexao();
					PreparedStatement pstmt = conexao
							.prepareStatement("UPDATE torneio SET nome_torneio = ?, descricao = ?, premiacao = ?, local = ?" +
							 " WHERE id = ?");
							 
					                  pstmt.setLong(1, torneio.getId());  
					                  pstmt.setString(2, torneio.getNomeTorneio());
					                  pstmt.setString(3, torneio.getDescricaoTorneio());
					                  pstmt.setString(4, torneio.getPremioTorneio());
									  pstmt.setString(5, torneio.getLocalTorneio());
						
					pstmt.execute();
					pstmt.close();
					conexao.close();
		 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		 
			public void excluirtorneio(Torneio torneio) {
				try {
					Connection conexao = getConexao();
					PreparedStatement pstm = conexao
							.prepareStatement("Delete from	torneio where id = ? ");
					pstm.setLong(1, torneio.getId());
					pstm.execute();
					pstm.close();
					conexao.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		 
			public boolean existetorneio(Torneio torneio) {
				boolean achou = false;
				try {
					Connection conexao = getConexao();
					PreparedStatement pstm = conexao
							.prepareStatement("Select * from torneio where id =	?");
					pstm.setLong(1, torneio.getId());
					ResultSet rs = pstm.executeQuery();
					if (rs.next()) {
						achou = true;
					}
					pstm.close();
					conexao.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return achou;
			}
		 
			public boolean inserirtorneio(Torneio torneio) {
				try {
					
					Connection conexao = getConexao();
					PreparedStatement pstm = conexao
							.prepareStatement("insert into torneio(nome_torneio, descricao, premiacao, local)"
				               		+ "values(?,?,?,?)");
					pstm.setString(1, torneio.getNomeTorneio());
		            pstm.setString(2, torneio.getDescricaoTorneio());
		            pstm.setString(3, torneio.getPremioTorneio());
		            pstm.setString(4, torneio.getLocalTorneio());
		            
		            pstm.execute();
					pstm.close();
					conexao.close();
					
					return true;
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				return false;
			}
		 
			public List<Torneio> listartorneio() {
				List<Torneio> lista = new ArrayList<Torneio>();
				try {
					Connection conexao = getConexao();
					Statement stm = conexao.createStatement();
					ResultSet rs = stm.executeQuery("Select * from torneio");
					while (rs.next()) {
						 Torneio torneio = new Torneio();
		                 torneio.setId(rs.getLong("id"));
		                 torneio.setNomeTorneio(rs.getString(2));
		                 torneio.setDescricaoTorneio(rs.getString(3));
		                 torneio.setPremioTorneio(rs.getString(4));
		                 torneio.setLocalTorneio(rs.getString(5));			
		                 lista.add(torneio);
					}
					stm.close();
					conexao.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return lista;
			}
		 
			public Torneio consultartorneio(Torneio torneio) {
				try {
					Connection conexao = getConexao();
					PreparedStatement pstm = conexao
							.prepareStatement("Select * from torneio where id =	?");
					pstm.setLong(1, torneio.getId());
					ResultSet rs = pstm.executeQuery();
					if (rs.next()) {
						torneio.setId(rs.getLong("id"));
						torneio.setNomeTorneio(rs.getString("nome_torneio"));
						torneio.setDescricaoTorneio(rs.getString("cpf"));
						torneio.setPremioTorneio(rs.getString("nome_fantasia"));
						torneio.setLocalTorneio(rs.getString("telefone"));
					
					}
					
					pstm.close();
					conexao.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return torneio;
			}
		}

		

		
		

	
	

